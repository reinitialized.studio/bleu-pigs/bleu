# Bleu
Bleu is an Open Source Discord<->ROBLOX Autonomous Community Management system built to manage the Bleu Pigs Scripting Community. As an Open Source project, you are free to take this and implement it in your own Community/contribute to the Project. 

## What does Bleu do?
Bleu has one goal:
To allow the Community to be autonomous while ensuring everyone is treated fairly and equal as possible

You can find version specific goals under [home/versions](https://gitlab.com/bleu-pigs/bleu/tree/home/versions)

## Source and other
Looking for the project Source? You can find the latest cutting edge releases on the [indev branch](https://gitlab.com/bleu-pigs/bleu/tree/indev), or the most stable on [stable](https://gitlab.com/bleu-pigs/bleu/tree/stable)
